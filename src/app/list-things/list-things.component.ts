import { Component, OnInit, HostBinding, Output, EventEmitter } from '@angular/core';
import { Thing } from '../models/thing.model';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-list-things',
  templateUrl: './list-things.component.html',
  styleUrls: ['./list-things.component.css']
})

export class ListThingsComponent implements OnInit {
  @HostBinding('attr.class') cssClass = 'col-md-8';
  fg: FormGroup;
  nombre: string;
  listthings: Thing[];
  @Output() onItemAdded: EventEmitter<Thing>;

  constructor(fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.nombre = 'Lista de objetos';
    this.listthings = [];
    this.fg = fb.group({ name: [''], url: [''] });
  }

  ngOnInit() {
  }

  guardar(thing: string) {
    let t = new Thing(thing, '');
    this.listthings.push(t);
    this.onItemAdded.emit(t);
    console.log('add item: ' + this.listthings);
    return false;
  }

}
